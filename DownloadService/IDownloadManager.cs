﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DownloadService
{
    public interface IDownloadManager
    {
        //Task DownloadAppAsync(string appname, CancellationToken cancellationToken);
        Task DownloadAppAsync(string url, CancellationToken cancellationToken, IProgress<int> progress = null);
        //Task DownloadAppAsync(string appname, IProgress<int> progress, CancellationToken cancellationToken);
        Task DownloadDeviceDriverAsync(string deviceDriverName, CancellationToken none, Progress<int> progress = null);
    }
}