﻿using System;
using System.Windows.Forms;
using caalhp.IcePluginAdapters;
using Exception = Ice.Exception;

namespace DownloadService
{
    public class Program
    {
        private static ServiceAdapter _adapter;
        private static DownloadServiceImplementation _implementation;

        static void Main(string[] args)
        {


            const string endpoint = "localhost";
            try
            {
                _implementation = new DownloadServiceImplementation();
                _adapter = new ServiceAdapter(endpoint, _implementation);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            Console.WriteLine("Press <ENTER> to exit program.");
            Console.ReadLine();

        }

    }
}
