namespace DownloadService
{
    public class DownloadParams
    {
        public string Address { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string RemoteDirectory { get; set; }
        public string DownloadDir { get; set; }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="other"></param>
        public DownloadParams(DownloadParams other)
        {
            Address = other.Address;
            Port = other.Port;
            Username = other.Username;
            Password = other.Password;
            RemoteDirectory = other.RemoteDirectory;
            DownloadDir = other.DownloadDir;
        }

        /// <summary>
        /// default constructor
        /// </summary>
        public DownloadParams()
        {
            
        }
    }
}