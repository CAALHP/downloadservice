﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using Newtonsoft.Json;
using System.Configuration;
using caalhp.Core.Utils.Helpers;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Diagnostics;

namespace DownloadService
{
    public class HttpDownloader
    {
        private IProgress<int> _progress;
        private bool _downloadComplete;
        
        private readonly WebClient _webClient;
        private readonly Uri _baseAddress;

        public HttpDownloader()
        {
            // web client
            _webClient = new CookieWebClient();

            //_baseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings.Get("MarketAddress"));
            _baseAddress = new Uri("https://carestoremarket.azurewebsites.net");
            // Login into the website (at this point the Cookie Container will do the rest of the job for us, and save the cookie for the next calls)


            AuthorizeCaalhp();
        }

        private void AuthorizeCaalhp()
        {
            //System.Diagnostics.Debugger.Launch();
            var relativeUri = "api/caalhp/authorize/" + CaalhpConfigHelper.GetCaalhpId();
            var authUri = new Uri(_baseAddress, relativeUri);
            var nameValues = new NameValueCollection();
            _webClient.UploadValues(authUri, "POST", nameValues);
        }


        public void DownloadFile(string url, string targetDirectory, CancellationToken cancellationToken,
            IProgress<int> progress = null)
        {
            if (url == null) throw new ArgumentNullException("url");
            var link = new Uri(url);
            _progress = progress;
            _downloadComplete = false;
            //var webClient = new WebClient();
            _webClient.DownloadProgressChanged += webClient_DownloadProgressChanged;
            _webClient.DownloadFileCompleted += webClient_DownloadFileCompleted;
            _webClient.DownloadFileAsync(link, Path.Combine(targetDirectory, Path.GetFileName(url)), cancellationToken);
            while (!_downloadComplete)
            {
                Thread.Sleep(100);
            }
            //var result = JsonConvert.DeserializeObject<IList<AppItem>>(content);
            //return result;
        }

        void webClient_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            _downloadComplete = true;
        }

        void webClient_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            double percentCompleted = (e.BytesReceived / e.TotalBytesToReceive) * 100;
            _progress.Report((int)percentCompleted);
        }
    }

    public class CookieWebClient : WebClient
    {
        public CookieContainer CookieContainer { get; private set; }

        /// <summary>
        /// This will instanciate an internal CookieContainer.
        /// </summary>
        public CookieWebClient()
        {
            CookieContainer = new CookieContainer();
        }

        /// <summary>
        /// Use this if you want to control the CookieContainer outside this class.
        /// </summary>
        public CookieWebClient(CookieContainer cookieContainer)
        {
            CookieContainer = cookieContainer;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = base.GetWebRequest(address) as HttpWebRequest;
            if (request == null) return base.GetWebRequest(address);
            request.CookieContainer = CookieContainer;
            return request;
        }
    }

    public class ContentType
    {
        public const string Json = "application/json";
    }
}
