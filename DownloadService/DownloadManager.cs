﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace DownloadService
{
    public class DownloadManager : IDownloadManager
    {
        /*private readonly string _address;
        private readonly int _port;
        private readonly string _username;
        private readonly string _password;
        private readonly string _remoteDirectory;
        private readonly string _downloadDir;*/
        private readonly DownloadParams _downloadParams;
        private const string Apps = "/apps/";
        private const string Drivers = "/drivers/";
        private const string Services = "/services/";

        public DownloadManager()
        {
            _downloadParams = new DownloadParams
            {
                Address = ConfigurationManager.AppSettings.Get("host"),
                Port = int.Parse(ConfigurationManager.AppSettings.Get("port")),
                Username = ConfigurationManager.AppSettings.Get("username"),
                Password = ConfigurationManager.AppSettings.Get("password"),
                RemoteDirectory = ConfigurationManager.AppSettings.Get("remoteDirectory"),
                DownloadDir =
                    Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                        ConfigurationManager.AppSettings.Get("downloadDirectory"))
            };

            var downloadDirInfo = new DirectoryInfo(_downloadParams.DownloadDir);
            if (!downloadDirInfo.Exists)
            {
                downloadDirInfo.Create();
            }
        }

        public async Task DownloadAppAsync(string url, CancellationToken cancellationToken,  IProgress<int> progress = null)
        {
            var parameters = new DownloadParams(_downloadParams)
            {
                RemoteDirectory = _downloadParams.RemoteDirectory + Apps
            };
            var downloader = new HttpDownloader();
            //await Task.Run(() => SFTPHelper.GetFile(parameters, appname, cancellationToken, progress), cancellationToken);
            await Task.Run(() => downloader.DownloadFile(url, parameters.DownloadDir, cancellationToken, progress), cancellationToken);
        }

        public async Task DownloadDeviceDriverAsync(string url, CancellationToken cancellationToken, Progress<int> progress = null)
        {
            var parameters = new DownloadParams(_downloadParams)
            {
                RemoteDirectory = _downloadParams.RemoteDirectory + Drivers
            };
            var downloader = new HttpDownloader();
            //await Task.Run(() => SFTPHelper.GetFile(parameters, deviceDriverName, cancellationToken, progress), cancellationToken);
            await Task.Run(() => downloader.DownloadFile(url, parameters.DownloadDir, cancellationToken, progress), cancellationToken);
        }
    }
}