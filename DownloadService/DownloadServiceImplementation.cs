﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Media;
using System.Threading;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;

namespace DownloadService
{
    public class DownloadServiceImplementation : IServiceCAALHPContract
    {
        private IServiceHostCAALHPContract _host;
        private int _processId;
        private IDownloadManager _downloadManager;

        public DownloadServiceImplementation()
        {

        }
        
        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(DownloadAppEvent e)
        {
            DownloadApp(e);
        }

        private void HandleEvent(DownloadDeviceDriverEvent e)
        {
            DownloadDeviceDriver(e);
        }

        private async void DownloadDeviceDriver(DownloadDeviceDriverEvent e)
        {
            if (String.IsNullOrWhiteSpace(e.DeviceDriverFileName)) return;
            PlayStartDownloadSound();
            var progress = new Progress<int>(i => DownloadProgress(i, e.DeviceDriverFileName));
            await _downloadManager.DownloadDeviceDriverAsync(e.DeviceDriverFileName, CancellationToken.None, progress);
            var command = new DownloadDeviceDriverCompletedEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                DeviceDriverFileName = e.DeviceDriverFileName//Path.GetFileName(e.DeviceDriverFileName)
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            _host.Host.ReportEvent(serializedCommand);
        }

        private void PlayStartDownloadSound()
        {
            SystemSounds.Beep.Play();
        }

        private async void DownloadApp(DownloadAppEvent e)
        {
            if (String.IsNullOrWhiteSpace(e.FileName)) return;
            PlayStartDownloadSound();
            var progress = new Progress<int>(i => DownloadProgress(i, e.FileName));
            await _downloadManager.DownloadAppAsync(e.FileName, CancellationToken.None, progress);
            var command = new DownloadAppCompletedEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                FileName = Path.GetFileName(e.FileName),
                AppName = e.AppName
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            _host.Host.ReportEvent(serializedCommand);
        }

        private void DownloadProgress(int progress, string filename)
        {
            //var command = EventHelper.CreateEvent(_processId, GetName(), "DownloadProgress", filename + ";" + progress.ToString(CultureInfo.InvariantCulture));
            var command = new DownloadProgressEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                PercentDone = progress,
                FileName = filename,
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            _host.Host.ReportEvent(serializedCommand);
        }

        public string GetName()
        {
            return "DownloadService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _downloadManager = new DownloadManager();
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadAppEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadDeviceDriverEvent)), _processId);
            //_host.Host.SubscribeToEvents(_processId);
        }

        public void Start()
        {
            //throw new System.NotImplementedException();
        }

        public void Stop()
        {
            //throw new System.NotImplementedException();
        }
    }
}