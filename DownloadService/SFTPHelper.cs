﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Renci.SshNet;
using Renci.SshNet.Sftp;

namespace DownloadService
{
    public class SFTPHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="remoteDirectory"></param>
        /// <returns></returns>
        public static List<string> GetFileNames(string host, int port, string username, string password, string remoteDirectory)
        {
            var list = new List<string>();
            var conInfo = new ConnectionInfo(host, port, username, new AuthenticationMethod[] { new PasswordAuthenticationMethod(username, password) });
            using (var sftp = new SftpClient(conInfo))
            {
                sftp.Connect();
                var files = sftp.ListDirectory(remoteDirectory);
                list.AddRange(from file in files where file.Name != "." && file.Name != ".." select file.Name);
            }
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="remoteDirectory"></param>
        /// <param name="filename"></param>
        /// <param name="targetDirectory"></param>
        /// <param name="progress"></param>
        /// <returns></returns>
        public static void GetFile(string host, int port, string username, string password,
            string remoteDirectory, string filename, string targetDirectory, IProgress<int> progress = null)
        {

            var conInfo = new ConnectionInfo(
                host,
                port,
                username,
                new AuthenticationMethod[] { new PasswordAuthenticationMethod(username, password) });
            using (var sftp = new SftpClient(conInfo))
            {
                sftp.Connect();
                var remotefile = remoteDirectory + filename;
                var remoteFileInfo = sftp.Get(remotefile);
                if(File.Exists(targetDirectory + filename)) //remove file to avoid overwrite errors
                    File.Delete(targetDirectory + filename);
                var file = File.Create(targetDirectory + filename);
                //var asynch = sftp.BeginDownloadFile(remotefile, file, null);
                var asynch = sftp.BeginDownloadFile(remotefile, file, null);
                var sftpAsynch = asynch as SftpDownloadAsyncResult;
                while (sftpAsynch != null && !sftpAsynch.IsCompleted)
                {

                    var percentCompleted = (sftpAsynch.DownloadedBytes / (double)remoteFileInfo.Length) * 100;
                    if (progress != null)
                        progress.Report((int)percentCompleted);
                    Thread.Sleep(100);
                }
                sftp.EndDownloadFile(asynch);
                file.Close();
                sftp.Disconnect();
            }
        }
        
        public static void GetFile(DownloadParams downloadParams, string filename, CancellationToken cancellationToken, IProgress<int> progress = null)
        {
            GetFile(downloadParams.Address, downloadParams.Port, downloadParams.Username, downloadParams.Password, downloadParams.RemoteDirectory, filename, downloadParams.DownloadDir, progress);
        }
    }
}
